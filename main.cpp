#include <array>
#include <thread>
#include <atomic>

#include <fmt/printf.h>


// scenario: single producer and consumer
// concept: lockless and no atomics. Managed through 3 indexes that refer to the storage. Each index is only updated by
//          one thread at most. read_position is written by the consumer, allocate_position and write_position both are
//          written by the producer. Reading old values has no critical impact on the system.
//
//          read_position is the slot which will be read next. The slot below is a dead slot and cannot be written
//          to long as as read_position remains the same.
//
//          write_position is the most recent fully written slot (aka. the last one that is ready for read)
//
//          allocate_position is the slot that has been allocated already but might not yet be fully initialized.
//
//    +-----+
//  5 | ... | <-- allocate_position
//    +-----+
//  4 | D3  | <-- write_position
//    +-----+
//  3 | D2  |
//    +-----+
//  2 | D1  |
//    +-----+
//  1 | D0  | <-- read_position
//    +-----+
//  0 | xxx | "dead" slot (aka the most recently read slot - cannot be used for writing)
//    +-----+
//

// --- RINGBUFFER -----------------------------------------------------------------------------------------------------

template<int N>
struct is_power_of_two : std::bool_constant<!(N & 1) && is_power_of_two<N / 2>::value>
{
};

template<>
struct is_power_of_two<1> : std::true_type
{
};

template<int N> constexpr auto is_power_of_two_v = is_power_of_two<N>::value;

template<typename T, std::size_t N>
class ringbuffer;

template<typename>
struct is_ringbuffer : std::false_type
{
};

template<typename T, std::size_t N>
struct is_ringbuffer<ringbuffer<T, N>> : std::true_type
{
};

template<typename T> constexpr auto is_ringbuffer_v = is_ringbuffer<T>::value;

template<typename Ringbuffer, typename = std::enable_if_t<is_ringbuffer_v<Ringbuffer>>>
class ringbuffer_consumer
{
public:
  using ringbuffer_type = Ringbuffer;

  class pop_operation
  {
  public:
    using value_type = typename ringbuffer_type::value_type;
    using pointer_type = value_type *;
    using reference_type = value_type &;

    pop_operation(ringbuffer_type &buffer) noexcept
      : buffer_{buffer}, slot_{buffer_.current_read_slot()}
    { }

    ~pop_operation() noexcept
    {
      buffer_.read_slot();
    }

    pointer_type operator->() const noexcept
    {
      return &slot_;
    }

    reference_type operator*() const noexcept
    {
      return slot_;
    }

  private:
    ringbuffer_type &buffer_;
    value_type &slot_;
  };

  explicit ringbuffer_consumer(ringbuffer_type &buffer)
    : buffer_{buffer}
  { }

  inline auto ready_for_read() const noexcept
  {
    return (buffer_.write_position_ + 1) % buffer_.slot_count != buffer_.read_position_;
  }

  pop_operation next() noexcept
  {
    return {buffer_};
  }

private:
  ringbuffer_type &buffer_;
};

template<typename Ringbuffer, typename = std::enable_if_t<is_ringbuffer_v<Ringbuffer>>>
class ringbuffer_producer
{
public:
  using ringbuffer_type = Ringbuffer;

  class push_operation
  {
  public:
    using value_type = typename ringbuffer_type::value_type;
    using pointer_type = value_type *;

    push_operation(ringbuffer_type &buffer) noexcept
      : buffer_{buffer}, slot_{buffer_.allocate_slot()}
    { }

    ~push_operation() noexcept
    {
      buffer_.write_slot();
    }

    pointer_type operator->() const noexcept
    {
      return &slot_;
    }

  private:
    ringbuffer_type &buffer_;
    value_type &slot_;
  };

  explicit ringbuffer_producer(ringbuffer_type &buffer) noexcept
    : buffer_{buffer}
  { }

  push_operation prepare() noexcept
  {
    return {buffer_};
  }

  inline auto ready_for_write() const noexcept
  {
    return (buffer_.allocate_position_ + 2) % buffer_.slot_count != buffer_.read_position_;
  }

private:
  ringbuffer_type &buffer_;
};

template<typename T, std::size_t N>
class ringbuffer
{
  friend class ringbuffer_producer<ringbuffer>;

  friend class ringbuffer_consumer<ringbuffer>;

public:
  using value_type = T;
  using reference_type = std::add_lvalue_reference_t<value_type>;
  static constexpr auto slot_count = N;

  using consumer_type = ringbuffer_consumer<ringbuffer>;
  using producer_type = ringbuffer_producer<ringbuffer>;

  template<typename R>
  class ptr
  {
  public:
    using value_type = R;
    using pointer_type = value_type *;
    using reference_type = value_type &;

    template<typename... Ts>
    explicit ptr(ptr<value_type> &owner, value_type *resource)
      : owner_{&owner}, resource_{resource}
    { }

    ptr(ptr const &) = delete;
    ptr &operator=(ptr const &) = delete;

    ptr(ptr &&other) noexcept
    {
      *this = std::move(other);
    }

    ptr &operator=(ptr &&other) noexcept
    {
      owner_ = other.owner_;
      resource_ = other.resource_;
      other.resource_ = nullptr;

      return *this;
    }

    ~ptr()
    {
      if (!resource_)
        return;

      *owner_ = std::move(*this);
    }

    pointer_type operator->() noexcept
    {
      return resource_;
    }

    reference_type operator*() noexcept
    {
      return *resource_;
    }

  private:
    ptr<value_type> *owner_;
    value_type *resource_;
  };

  static_assert(slot_count > 1, "slot_count must be greater than one (effective slot count would be zero)");
  static_assert(is_power_of_two_v<slot_count>, "slot_count should be a power of two for performance reasons");

  ringbuffer()
    : consumer_{*this}, consumer_ptr_{consumer_ptr_, &consumer_}, producer_{*this},
      producer_ptr_{producer_ptr_, &producer_}
  { }

  auto producer() noexcept
  {
    return std::move(producer_ptr_);
  }

  auto consumer() noexcept
  {
    return std::move(consumer_ptr_);
  }

private:
  std::atomic_size_t read_position_ = 1;
  std::atomic_size_t allocate_position_ = 0;
  std::atomic_size_t write_position_ = 0;

  consumer_type consumer_;
  ptr<consumer_type> consumer_ptr_;

  producer_type producer_;
  ptr<producer_type> producer_ptr_;

  std::array<value_type, slot_count> data_{};

  inline reference_type allocate_slot() noexcept
  {
    std::size_t pos = allocate_position_;
    ++pos %= slot_count;
    allocate_position_ = pos;
    return data_[allocate_position_];
  }

  inline void write_slot() noexcept
  {
    write_position_ = static_cast<std::size_t>(allocate_position_);
  }

  inline void read_slot() noexcept
  {
    std::size_t pos = read_position_;
    ++pos %= slot_count;
    read_position_ = pos;
  }

  inline reference_type current_read_slot() noexcept
  {
    return data_[read_position_];
  }
};

// --- LOGGER ---------------------------------------------------------------------------------------------------------

template<std::size_t Size>
struct message_slot
{
  using buffer_t = std::array<char, Size>;
  using fptr_t = void(message_slot<Size> &);

  static constexpr auto storage_size = Size;

  char const *format;
  fptr_t *fptr;
  alignas(Size) buffer_t buffer;
};

template<typename Producer, typename... Ts>
bool log(Producer &producer, char const *format, Ts &&... params) noexcept
{
  using tuple_type = std::tuple<std::remove_reference_t<Ts>...>;

  constexpr auto storage_size = Producer::value_type::ringbuffer_type::value_type::storage_size;
  static_assert(sizeof(tuple_type) <= storage_size, "log message parameter storage capacity exceeded");

  if (!producer->ready_for_write())
    return false;

  auto slot = producer->prepare();

  new(&slot->buffer) tuple_type{std::forward<Ts>(params)...};
  slot->format = format;

  slot->fptr = [](message_slot<storage_size> &slot)
  {
    auto &params = *reinterpret_cast<tuple_type *>(&slot.buffer);

    std::apply([&](auto &&... params)
               {
                 fmt::print(slot.format, std::forward<decltype(params)>(params)...);
               }, params);
  };

  return true;
}

// --- DUMMY CODE -----------------------------------------------------------------------------------------------------

template<typename Producer>
void producer_task(Producer producer)
{
  using namespace std::chrono_literals;

  std::size_t dropped_messages = 0;
  for (auto i = 0u; i < 1000; ++i) {
    if (!log(producer, "[{}] some log message in rt context (dropped {} messages)\n", i, dropped_messages))
      ++dropped_messages;
    else
      dropped_messages = 0;

    std::this_thread::sleep_for(10us);
  }
}

template<typename Consumer>
void consumer_task(Consumer consumer, bool &keep_running)
{
  using namespace std::chrono_literals;

  while (keep_running) {
    if (!consumer->ready_for_read()) {
      std::this_thread::sleep_for(5ms);
      continue;
    }

    auto slot = consumer->next();
    slot->fptr(*slot);
  }
}

int main()
{
  ringbuffer<message_slot<64>, 32> buffer{};

  bool keep_running = true;
  std::thread logging_thread{[&]() { consumer_task(buffer.consumer(), keep_running); }};

  std::thread realtime_thread{[&]() { producer_task(buffer.producer()); }};

  fmt::print("Wait for realtime task to finish\n");
  realtime_thread.join();

  keep_running = false;
  fmt::print("Wait for logging task to finish\n");
  logging_thread.join();
}

// --- DUMMY CODE -----------------------------------------------------------------------------------------------------